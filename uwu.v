module uwu

import os

pub const app_name = os.base(os.args[0]).all_before('.')

// get_args return the command-line arguments.
@[inline]
pub fn get_args() []string {
	return os.args[1..]
}

// need_args ensures that the user have passed at least
// the number of command-line arguments to the program.
pub fn need_args(min int) ![]string {
	args := get_args()
	if args.len >= min {
		return args
	}
	return error('not enough arguments: expecting ${min}, got ${args.len}')
}

@[inline]
pub fn get_env(name string) string {
	return os.getenv(name)
}

@[inline]
pub fn need_env(name string) !string {
	val := get_env(name)
	if val.len > 0 {
		return val
	}
	return error('environment variable ${name} has no value')
}
