import uwu

fn test_get_exe() {
	if cmd := uwu.get_exe('not-a-cmd') {
		assert false, 'command `not-a-cmd` should not be in PATH'
	} else {
		assert true
	}
}

fn test_need_exe() {
	if cmd := uwu.get_exe('v') {
		out := cmd.exec('-v')!
		assert out.len > 0
	} else {
		assert false, 'command `v` should be in PATH'
	}
}
