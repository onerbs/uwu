module uwu

import os

const chunk_size = 512

fn read_bytes(fil os.File) ![]u8 {
	mut res := []u8{}
	mut buf := []u8{len: chunk_size}
	for {
		len := fil.read(mut buf)!
		if len < chunk_size {
			res << buf[..len]
			break
		}
		res << buf
	}
	return res
}

// fn C.getc(&C.FILE) int

// fn read_text(file os.File) !string {
// 	fil := unsafe { &C.FILE(file.cfile) }
// 	mut buf := []u8{}
// 	for {
// 		byt := C.getc(fil)
// 		if byt < 0 {
// 			if C.ferror(fil) != 0 {
// 				return error('file read error')
// 			}
// 			break
// 		}
// 		match byt {
// 			13 {}
// 			else {
// 				buf << u8(byt)
// 			}
// 		}
// 	}
// 	return buf.bytestr()
// }
