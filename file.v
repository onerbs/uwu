module uwu

import os

@[inline]
pub fn read_file(name string) ![]u8 {
	mut fil := os.open_file(name, 'rb')!
	defer {
		fil.close()
	}
	return read_bytes(fil)
}

@[inline]
pub fn read_text(name string) !string {
	mut fil := os.open_file(name, 'r')!
	defer {
		fil.close()
	}
	return read_bytes(fil)!.bytestr()
}

@[inline]
pub fn write_file(name string, buf []u8) !int {
	mut fil := os.open_file(name, 'wb')!
	defer {
		fil.close()
	}
	return fil.write(buf)
}

@[inline]
pub fn write_text(name string, str string) !int {
	mut fil := os.open_file(name, 'w')!
	defer {
		fil.close()
	}
	return fil.write_string(str)
}

//------------------------------------------------

@[inline]
pub fn get_text() string {
	if buf := read_bytes(os.stdin()) {
		return buf.bytestr()
	} else {
		eprintln(err)
	}
	return ''
}
