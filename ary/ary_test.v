import uwu.ary

fn test_chunk() {
	input := [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

	mut tmp := [[0, 1], [2, 3], [4, 5], [6, 7], [8, 9]]
	assert ary.chunk(input, 2) == tmp

	tmp = [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9]]
	assert ary.chunk(input, 3) == tmp

	tmp = [[0, 1, 2, 3], [4, 5, 6, 7], [8, 9]]
	assert ary.chunk(input, 4) == tmp
}

fn test_flat() {
	input := [[0, 1], [2, 3], [4, 5], [6, 7], [8, 9]]
	assert ary.flat(input) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

	input2 := [[[0, 1], [2, 3]], [[4, 5], [6, 7]], [[8, 9]]]
	assert ary.flat(input2) == input
	assert ary.flat(ary.flat(input2)) == [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
}
