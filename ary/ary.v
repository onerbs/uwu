module ary

// chunk split an array into several arrays of the specified size.
// the last element may contain
pub fn chunk[T](ary []T, size int) [][]T {
	mut res := [][]T{}
	for ix in 0 .. ary.len {
		if ix % size == 0 {
			res << []T{}
		}
		res[res.len - 1] << ary[ix]
	}
	return res
}

// flat
pub fn flat[T](ary [][]T) []T {
	mut res := []T{}
	for chunk in ary {
		res << chunk
	}
	return res
}
