import uwu.buffer

fn test_from_str() {
	str := '  \tahoy\n❤️\n\t  \v'
	mut buf := buffer.from_str(str)
	assert buf.len == str.len
	assert buf.str() == str
}

fn test_strip() {
	mut buf := buffer.new()
	buf.write('  \tahoy\n\n\t  \v')
	assert buf.strip() == 'ahoy'

	buf.write('  \t\n\n\t  \v')
	assert buf.strip() == ''
}
