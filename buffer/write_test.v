import uwu.buffer

fn test_write() {
	mut buf := buffer.cap(10)
	buf.write('ahoy ')
	buf.write('ahoy')
	assert buf.str() == 'ahoy ahoy'

	buf.write('ahoy')
	assert buf.str() == 'ahoy'
}

fn test_writeln() {
	mut buf := buffer.cap(10)
	buf.writeln('ahoy')
	buf.writeln('ahoy')
	assert buf.str() == 'ahoy\nahoy\n'

	buf.writeln('ahoy')
	assert buf.str() == 'ahoy\n'
}
