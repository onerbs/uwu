module log

import uwu.style

@[inline]
pub fn text(a ...string) {
	match a.len {
		0 {}
		1 {
			t := style.tag('   •', .bold, .white)
			eprintln('${t} ${a[0]}')
		}
		2 {
			t := style.tag(a[0], .bold, .white)
			eprintln('${t} ${a[1]}')
		}
		else {
			mut x := a.clone()
			t := style.tag(x[0], .bold, .white)
			x.delete(0)
			eprintln('${t} ${x.join(': ')}')
		}
	}
}

@[inline]
pub fn done(a ...string) {
	t := style.tag(@FN, .bold, .green)
	eprintln('${t} ${a.join(': ')}')
}

@[inline]
pub fn info(a ...string) {
	t := style.tag(@FN, .bold, .yellow)
	eprintln('${t} ${a.join(': ')}')
}

@[inline]
pub fn warn(a ...string) {
	t := style.tag(@FN, .bold, .purple)
	eprintln('${t} ${a.join(': ')}')
}

@[inline]
pub fn fail(a ...string) {
	t := style.tag(@FN, .bold, .red)
	eprintln('${t} ${a.join(': ')}')
}
