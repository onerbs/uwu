module style

@[inline]
pub fn mark(s string, style ...Style) string {
	return tag(s.to_upper(), ...style)
}

@[inline]
pub fn tag(s string, style ...Style) string {
	return tint(' ${s} ', ...style)
}

@[inline]
pub fn tint(s string, style ...Style) string {
	mut res := s
	for mod in style {
		res = mod.apply(res)
	}
	return res
}
