module style

pub enum Style {
	plain       = 0
	bold
	dim
	italic
	underline
	blink
	rapid_blink
	invert
	hidden
	strike
	//
	black       = 30
	white
	red
	orange
	yellow
	green
	cyan
	blue
	purple
	pink
	//
	over_black  = 40
	over_white
	over_red
	over_orange
	over_yellow
	over_green
	over_cyan
	over_blue
	over_purple
	over_pink
}

fn (self Style) rgb_color() (int, int, int) {
	return match self {
		// vfmt off
		.black,  .over_black  { 0x20, 0x1f, 0x1f }
		.white,  .over_white  { 0xe2, 0xdf, 0xdf }
		.red,    .over_red    { 0xed, 0x53, 0x53 }
		.orange, .over_orange { 0xff, 0xa1, 0x54 }
		.yellow, .over_yellow { 0xff, 0xe1, 0x6b }
		.green,  .over_green  { 0x9b, 0xdb, 0x4d }
		.cyan,   .over_cyan   { 0x43, 0xd6, 0xb5 }
		.blue,   .over_blue   { 0x64, 0xba, 0xff }
		.purple, .over_purple { 0xcd, 0x9e, 0xf7 }
		.pink,   .over_pink   { 0xf4, 0x67, 0x9d }
		// vfmt on
		else { 0, 0, 0 }
	}
}

fn (self Style) apply(str string) string {
	mode := int(self)
	tent := mode / 10
	match tent {
		0 {
			end := match mode {
				0 { 0 }
				1 { 22 }
				else { 20 + mode }
			}
			return '\x1b[${mode}m${str}\x1b[${end}m'
		}
		else {
			r, g, b := self.rgb_color()
			return '\x1b[${tent}8;2;${r};${g};${b}m${str}\x1b[${tent}9m'
		}
	}
}
