module uwu

import uwu.log

// report print a fancy error message.
@[if ! quiet ?]
pub fn report(err IError) {
	msg := err.msg().trim_space()
	if msg.len > 0 {
		if err.code() > 0 {
			log.fail(msg)
		} else {
			log.text(uwu.app_name, msg)
		}
	}
}

// catch report an error and exit with the error status code.
@[inline; noreturn]
pub fn catch(err IError) {
	report(err)
	exit(err.code())
}

// die terminate the program execution with an error message.
@[inline; noreturn]
pub fn die(msg string) {
	catch(error_with_code(msg, 0xBAD))
}
