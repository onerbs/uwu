module cmd

import uwu.str
import os

// call execute a command and return it's output.
// this will throw an error if the return code is not zero.
pub fn call(args ...string) !string {
	cmd := args.map(str.safe_quote(it)).join(' ')
	return exec(cmd)
}

// exec execute a command and return it's output.
// this will throw an error if the return code is not zero.
pub fn exec(cmd string) !string {
	$if noop ? {
		return error(cmd)
	} $else {
		res := os.execute(cmd)
		out := res.output.trim_space()
		if res.exit_code != 0 {
			return error_with_code(out, res.exit_code)
		}
		return out
	}
}
