module cmd

import uwu
import os

@[noinit]
pub struct Order {
	args []string
}

// new create a new Order.
pub fn new(args ...string) Order {
	if self := get(...args) {
		return self
	}
	return Order{args}
}

// get create a new Order instance.
// it will return an error if the requested executable is not found.
pub fn get(args ...string) !Order {
	if args.len < 1 {
		return error('empty command name')
	}
	mut argv := args.clone()
	argv[0] = find_exe_path(args[0])!
	return Order{argv}
}

// need create a new Order instance.
// it will exit the program if the requested executable is not found.
@[inline]
pub fn need(args ...string) Order {
	return get(...args) or { uwu.catch(err) }
}

// call will call the executable with the provided arguments,
// capture and return the output.
// this will return an error if the execution fails.
pub fn (self Order) call(args ...string) !string {
	mut all_args := []string{cap: self.args.len + args.len}
	all_args << self.args
	all_args << args
	return cmd.call(...all_args)
}

// -----------------------------------------------

// find_exe_path return the absolute path to the executable or the executable name.
fn find_exe_path(name string) !string {
	if path := os.find_abs_path_of_executable(name) {
		return path
	}
	nam := if name.ends_with('.exe') { name#[..-4] } else { '${name}.exe' }
	if path := os.find_abs_path_of_executable(nam) {
		return path
	}
	return error('command "${name}" was not found')
}
